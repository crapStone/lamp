// This file is part of the "lamp" program.
//     Copyright (C) 2024  crapStone
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod cli;
mod controllers;

use std::{io::stdout, process::exit};

use clap_complete::{generate, shells};
use controllers::{Controller, LinController, LogController, RawController};

use crate::cli::build_cli;

const BIN_NAME: &str = env!("CARGO_PKG_NAME");

fn main() {
    let app = build_cli();
    let matches = app.get_matches();

    if let Some(shell_str) = matches.get_one::<String>("gen_completion").map(String::as_str) {
        let mut app = build_cli();

        match shell_str {
            "bash" => generate(shells::Bash, &mut app, BIN_NAME, &mut stdout()),
            "zsh" => generate(shells::Zsh, &mut app, BIN_NAME, &mut stdout()),
            "fish" => generate(shells::Fish, &mut app, BIN_NAME, &mut stdout()),
            _ => panic!("{ERROR_MSG}"),
        };

        return;
    }

    let (default_ctrl, ctrls) = controllers::get_controllers().unwrap_or_else(|why| {
        eprintln!("an error occured when reading devices: '{why}'");
        exit(exitcode::IOERR)
    });

    let p = match matches.get_one::<String>("controller") {
        Some(ctrl) => {
            let p = ctrls.get(ctrl);
            if p.is_none() {
                eprintln!("no device with name '{ctrl}' found");
                eprintln!("use --list to ge a list of all available devices");
                exit(exitcode::DATAERR);
            }

            p.unwrap().to_owned()
        }
        None => ctrls.get(&default_ctrl).unwrap().to_owned(),
    };

    let controller: Box<dyn Controller> = match matches.get_one::<String>("ctrl_type").map(String::as_str) {
        Some("raw") => Box::new(RawController::new(p)),
        Some("lin") => Box::new(LinController::new(p)),
        Some("log") => Box::new(LogController::new(p)),
        Some(_) | None => panic!("{ERROR_MSG}"),
    };

    if matches.get_flag("list") {
        println!("{default_ctrl} [default]");
        for ctrl in ctrls.keys() {
            if ctrl != &default_ctrl {
                println!("{ctrl}");
            }
        }
        exit(exitcode::OK);
    } else {
        match matches.subcommand() {
            Some(("set", sub_matches)) => {
                if let Some(value) = sub_matches.get_one("value") {
                    controller.set_brightness(*value);
                }
            }
            Some(("inc", sub_matches)) => {
                if let Some(value) = sub_matches.get_one("value") {
                    let new_value = controller.get_brightness() + value;
                    controller.set_brightness(new_value.min(controller.get_max_brightness()));
                }
            }
            Some(("dec", sub_matches)) => {
                if let Some(value) = sub_matches.get_one("value") {
                    let new_value = controller.get_brightness() - value;
                    controller.set_brightness(new_value);
                }
            }
            Some(("get", _)) => println!("{}", controller.get_brightness()),
            Some(("zero", _)) => controller.set_brightness(0),
            Some(("full", _)) => controller.set_brightness(controller.get_max_brightness()),
            _ => build_cli().print_long_help().unwrap(),
        }
    }
}

// https://xkcd.com/2200/
const ERROR_MSG: &str = r#"
    ERROR!

    If you're seeing this, the code is in what I thought was an unreachable state.

    I could give you advice for what to do. but honestly, why should you trust me?
    I clearly screwed this up. I'm writing a message that should never appear,
    yet I know it will probably appear someday.

    On a deep level, I know I'm not up to this task. I'm so sorry.
"#;
