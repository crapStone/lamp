// This file is part of the "lamp" program.
//     Copyright (C) 2024  crapStone
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use clap::{Arg, ArgAction, Command};

pub fn build_cli() -> Command {
    Command::new("lamp")
        .version(env!("CARGO_PKG_VERSION"))
        .author("crapStone <me@crapstone.dev>")
        .about("Utility to interact with backlight")
        .arg_required_else_help(true)
        .subcommand(
            Command::new("set").about("Sets brightness to given value").arg(
                Arg::new("value")
                    .num_args(1)
                    .value_parser(clap::value_parser!(u32))
                    .value_name("VALUE"),
            ),
        )
        .subcommand(
            Command::new("inc").about("Increase brightness").arg(
                Arg::new("value")
                    .num_args(1)
                    .value_parser(clap::value_parser!(u32))
                    .value_name("VALUE"),
            ),
        )
        .subcommand(
            Command::new("dec").about("Decrease brightness").arg(
                Arg::new("value")
                    .num_args(1)
                    .value_parser(clap::value_parser!(u32))
                    .value_name("VALUE"),
            ),
        )
        .subcommand(Command::new("get").about("Prints current brightness value"))
        .subcommand(Command::new("zero").about("Sets brightness to lowest value"))
        .subcommand(Command::new("full").about("Sets brightness to highest value"))
        .arg(
            Arg::new("list")
                .short('l')
                .long("list")
                .action(ArgAction::SetTrue)
                .help("Lists all devices with controllable brightness and led values")
                .exclusive(true),
        )
        .arg(
            Arg::new("controller")
                .short('c')
                .long("controller")
                .help("Select device to control, defaults to the first device found")
                .value_name("DEVICE")
                .num_args(1),
        )
        .arg(
            Arg::new("ctrl_type")
                .short('t')
                .long("type")
                .value_name("controller_type")
                .num_args(1)
                .value_parser(["raw", "lin", "log"])
                .default_value("lin")
                .help("choose controller type")
                .long_help(
                    r#"You can choose between these controller types:
raw: uses the raw values found in the device files
lin: uses percentage values (0.0 - 1.0) with a linear curve
log: uses percentage values (0.0 - 1.0) with a logarithmic curve
     the perceived brightness for the human eyes should be linear with this controller
"#,
                ),
        )
        .arg(
            Arg::new("gen_completion")
                .long("gen-completion")
                .hide(true)
                .num_args(1)
                .value_name("shell")
                .value_parser(["bash", "fish", "zsh"])
                .help("Sets shell to generate completions for"),
        )
}

#[cfg(test)]
mod test {
    #[test]
    fn verify_cli_compiles() {
        super::build_cli().debug_assert();
    }
}
