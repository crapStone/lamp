# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- new hidden flag `gen-completions` to generate shell completions to `stdout`

### Removed

- completions will no longer be generated during build time

## [0.3.1] - 2022-07-16

### Changed

- Code cleanup and better error messages

## [0.3.0] - 2022-07-16

### Added

- Use `-c` or `--controller` to select device to change brightness for

### Changed

- Improved README

[unreleased]: https://codeberg.org/crapStone/lamp/compare/v0.3.1...main
[0.3.1]: https://codeberg.org/crapStone/lamp/releases/tag/v0.3.1
[0.3.0]: https://codeberg.org/crapStone/lamp/releases/tag/v0.3.0
