# Lamp

[![status-badge](https://ci.codeberg.org/api/badges/crapStone/lamp/status.svg)](https://ci.codeberg.org/crapStone/lamp)
[![Crates.io](https://img.shields.io/crates/v/lamp.svg)](https://crates.io/crates/lamp)

Lamp is a backlight control program written in Rust and inspired by
[acpibacklight](https://gitlab.com/wavexx/acpilight).

## Features

In contrast to acpilight lamp is not backwards compatible with xbacklight.
It is intended to be used as a standalone replacement for new scripts.

```none
Commands:
    set     Sets brightness to given value
    inc     Increase brightness
    dec     Decrease brightness
    get     Prints current brightness value
    zero    Sets brightness to lowest value
    full    Sets brightness to highest value
    help    Print this message or the help of the given subcommand(s)

Options:
    -l, --list                      Lists all devices with controllable brightness and led values
    -c, --controller <DEVICE>       Select device to control, defaults to the first device found
    -t, --type <controller_type>    choose controller type [default: lin] [possible values: raw, lin, log]
    -h, --help                      Print help (see a summary with '-h')
    -V, --version                   Print version information
```

## Install

Binary packages for the following systems are currently available.

[![Packaging status](https://repology.org/badge/vertical-allrepos/lamp.svg)](https://repology.org/project/lamp/versions)

You can also install it via `cargo`:

```bash
cargo install lamp
```

You have to make sure, that you have write access to `/sys/class/backlight/`.
This can be achieved by using udev rules like `90-backlight.rules` in this repo.

## Build

lamp is a pure Rust project so you can simply run `cargo build` after installing the Rust toolchain.

Formatting is done via `cargo fmt` with the default rules and in the pipeline `clippy` is run with the following arguments:

```bash
cargo clippy -- --deny clippy::all --deny warnings
```
